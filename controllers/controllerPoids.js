const {Poids} = require('../config/sequelize')
const {Op} = require("sequelize");

exports.AjoutPoids = (req,res) =>{
    date=new Date(req.body.createdAt);
    if(date> new Date()){
        return res.status(400).send({message:"Vous ne pouvez pas enregistrer à une date ultérieure."})
    }
    Poids.findOne({
        where :{
            idUser:req.body.idUser,        
        createdAt: {
            [Op.lte]: date,
            [Op.gte]: new Date(date-24*60*60*1000)
        }}
    }).then((listPoids)=>{
        if(listPoids != null||undefined){
            if(listPoids.createdAt.getDate() ===date.getDate() &&
                listPoids.createdAt.getMonth() ===date.getMonth() &&
                listPoids.createdAt.getFullYear() ===date.getFullYear()){
                return res.status(400).send({message:"Vous avez déjà enregistré un poids à cette date"})
            }
        }else{
            try{
                Poids.create({
                    idUser:req.body.idUser,
                    poids:req.body.poids,
                    createdAt:req.body.createdAt
                }).then(poids =>{
                    res.send({message:"Le Poids a bien été enregistré",poids:poids})
                }) 
               }catch(error){
                res.status(500).send({error: error.message});
               }
        }
    })
   
   
}

exports.FindAllPoids = (req,res) =>{
    try{
        Poids.findAll({
            order:[['createdAt','ASC']],
            where: {
                idUser:req.params.idUser
            }
        }).then(listPoids =>{
            if(listPoids.length<=0){
                return res.status(400).send({message:"Vous n'avez pas entrer de données"})
            }else{
                res.status(200).send({message:"Voici les mesures de la personne ayant l'id "+req.params.id, poids:listPoids})
            }
        })
    }catch(error){
        res.status(500).send({error:error.message});
    }
}

exports.FindPoidsWeek = (req,res) =>{
    try{
        Poids.findAll({
            order:[['createdAt','ASC']],
            where :{
                idUser:req.params.idUser,        
            createdAt: {
                [Op.lte]: new Date(),
                [Op.gte]: new Date(new Date()-7*24*60*60*1000)
            }}
        }).then(listPoids =>{
            console.log(listPoids);
            if(listPoids.length<=0){
                return res.status(400).send({message:"Vous n'avez pas entrer de données"})
            }else{
                return res.status(200).send({message:"Voici les pesées de cette semaine",poids:listPoids})
            }
        })
    }catch(error){
        return res.status(500).send({error:error.message})
    }
}

exports.FindPoidsMonth = (req,res) =>{
    try{
        Poids.findAll({
            order:[['createdAt','ASC']],
            where :{
                idUser:req.params.idUser,        
            createdAt: {
                [Op.lte]: new Date(),
                [Op.gte]: new Date(new Date()-30*24*60*60*1000)
            }}
        }).then(listPoids =>{
            console.log(listPoids);
            if(listPoids.length<=0){
                return res.status(400).send({message:"Vous n'avez pas entrer de données ce mois-ci"})
            }else{
                return res.status(200).send({message:"Voici les pesées de ces 30 derniers jours",poids:listPoids})
            }
        })
    }catch(error){
        return res.status(500).send({error:error.message})
    }
}

exports.FindPoidsTrimester = (req,res) =>{
    try{
        Poids.findAll({
            where :{
                idUser:req.params.idUser,        
            createdAt: {
                [Op.lte]: new Date(),
                [Op.gte]: new Date(new Date()-90*24*60*60*1000)
            }}
        }).then(listPoids =>{
            console.log(listPoids);
            if(listPoids.length<=0){
                return res.status(400).send({message:"Vous n'avez pas entrer de données ce trimestre"})
            }else{
                return res.status(200).send({message:"Voici les pesées de ces 90 derniers jours",poids:listPoids})
            }
        })
    }catch(error){
        return res.status(500).send({error:error.message})
    }
}

exports.DeletePoids =(req,res) =>{
    try{
        Poids.destroy({
        where:{
            id:req.params.id
        }
    }).then((num)=>{
        if(num == 1){
            res.send({message:"Le poids que vous avez selectionné a bien été supprimé"})
        }
        else{
            res.send({message:`Nous ne pouvons pas effacer le tutoriel n° ${req.params.id}. Peut être qu'il n'existe pas`})
        }
    })
    }catch(error){
        res.status(500).send({error:error.message})
    }
}

exports.FindLastPoids = (req,res) =>{
    try{
        Poids.findOne({
            order:[
                ['createdAt','DESC']
            ],
            where:{
                idUser:req.params.idUser
            }
        }).then(listPoids =>{
            res.status(200).send({message:`voici le dernier poids de l'utilisateur n° ${req.params.idUser}`,poids:listPoids});
        })
    }catch(error){
        res.status(500).send({error: error.message})
    }
    
}