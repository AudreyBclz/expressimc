const {User} = require("../config/sequelize");
const {Poids} = require('../config/sequelize')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../config/auth.config.js');

exports.Inscription = (req,res)=>{
  User.findOne({
    where: {
      name:req.body.name
    }
  }).then(user =>{
    
      try{
        let {name,age,taille,password,poids} = req.body;
        bcrypt.hash(password,10).then((passhash)=> {
          User.create({
            name:name,
            age:age,
            taille:taille,
            password:passhash
          })
        .then((user)=>{
          console.log("teste fonction user : " +user);
          Poids.create({
            idUser:user.id,
            poids:poids,
            createdAt:new Date()
          }).then(
            res.send({message:"Utilisateur a bien été enregistré"})
          )
          
        });
      });
    }catch(error){
      res.status(500).send({error: error.message});
    }
  }
  )
}



exports.Connexion = function (req,res) {
  try{
    let { name,password} = req.body;

    User.findOne({
      where: {
        name:name
      },
    }).then((user) =>{
      if(!user){
        return res.status(400).send({message: "L'utilisateur n'a pas été trouvé"});
      }

      var passwordIsValid = bcrypt.compareSync(password,user.password);

      if(!passwordIsValid){
        return res.status(401).send({token:null,message:"Mot de passe incorrect"});
      }
      var token = jwt.sign({id:user.id}, config.secret, {
        expiresIn:60000,
      });
      res.send({
        data: {
          id:user.id,
          name:user.name,
          taille:user.taille,
          token:token,
        },
        status:200,
      });
    });
  }catch(error){
    res.status(500).send({error: error.message});
  }
};

exports.FindOneUser = (req,res) =>{
  let id = req.params.id;
  try{
    User.findOne({
      where :{
        id:id
      }
    }).then(user =>{
      if(user != undefined||null){
        res.send({
          data: user,
          status:200,
        });
      }else{
        return res.status(401).send({message:" L'utilisateur n'existe pas"})
      }
      
    })
  }catch{
    return res.status(500).send({error: error.message})
  }
}










//

