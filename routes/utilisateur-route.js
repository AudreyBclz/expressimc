
const express = require ("express");
const app = express();


const checkUpName = require('../middleware/verifyRegister');
const{ Inscription, Connexion, FindOneUser }=require("../controllers/controllerUser");
const{ 
    AjoutPoids, 
    FindPoidsWeek, 
    FindPoidsMonth, 
    FindPoidsTrimester, 
    FindAllPoids, 
    DeletePoids, 
    FindLastPoids,
}=require("../controllers/controllerPoids");
const router =express.Router();



router.post('/inscription',checkUpName,Inscription);
router.post('/connexion',Connexion);
router.post('/poids',AjoutPoids);
router.delete('/poids/:id',DeletePoids);
router.get('/:id',FindOneUser);
router.get('/poids/last/:idUser',FindLastPoids);
router.get('/poids/:idUser',FindAllPoids);
router.get('/poids/semaine/:idUser',FindPoidsWeek);
router.get('/poids/mois/:idUser',FindPoidsMonth);

router.get('/poids/trimestre/:idUser',FindPoidsTrimester);

app.use('/api/user',router);




module.exports =router;
//
