module.exports = (sequelize,Datatypes) => {
    return sequelize.define(
        "Poids",
        {
            id:{
                type: Datatypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            idUser:{
                type: Datatypes.INTEGER,
                allowNull:false,
            },
            poids:{
                type:Datatypes.FLOAT,
                allowNull:false
            },
            createdAt:{
                type: Datatypes.DATE,
                allowNull:false
            }

        },
        {
            createdAt:false,
            updatedAt:false
        }
    )
}