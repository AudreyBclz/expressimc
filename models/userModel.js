module.exports =(sequelize,DataTypes)=>{
  return sequelize.define(
    "User",
    {
      id:{
        type:DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement:true,
      },
      name:{
        type: DataTypes.STRING,
        allowNull: false,
        unique:{ msg:"Ce pseudo est déjà utilisé"}
      },
      age:{
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      taille: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      password:{
        type: DataTypes.STRING,
        allowNull:false
      }
    },
    {
        createdAt:false,
        updatedAt:false,
    }
  )
}

//
