const {  DataTypes, Sequelize } = require("sequelize");

const UserModel = require('../models/userModel');
const PoidsModel = require('../models/poidsModel')



const sequelize = new Sequelize("imc", "root", "", {
  host: "localhost",
  dialect: "mariadb",
  dialectOptions:{
    timezone:"ETC/GMT-2",
  },
  logging: false,
});

const User = UserModel(sequelize,DataTypes);
const Poids = PoidsModel(sequelize,DataTypes);


module.exports = {User,Poids};

