const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');
const indexRouter = require('./routes/utilisateur-route');

const sequelize = require('./config/sequelize');
const { SequelizeScopeError } = require("sequelize");
const app =express();

var corsOptions = {
  origin : "http://localhost:4200" // lien donnée et appli
};

app.use(cors(corsOptions)); // pour pouvoir utiliser cors

app.use(bodyParser.json());

app.use(function (req,res,next){
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, Origin, Content-Type, Accept"
  );
  next();
});

app.use("/api/user",indexRouter);



app.listen(4000,() =>{
  console.log("listening on port 4000");
});
//
